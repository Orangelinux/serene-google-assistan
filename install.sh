#!bin/bash
sudo apt update && sudo apt upgrade
sudo apt install python3-dev
sudo apt install python-pip
python -m pip install --upgrade google-assistant-sdk[samples]
python -m pip install --upgrade google-auth-oauthlib[tool]
google-oauthlib-tool --scope https://www.googleapis.com/auth/assistant-sdk-prototype \
      --save --headless --client-secrets ./google.json

